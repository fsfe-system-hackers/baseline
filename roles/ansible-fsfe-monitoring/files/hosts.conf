# SPDX-FileCopyrightText: 2020 Vincent Lequertier <vincent@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

/*
 * Host definitions with object attributes
 * used for apply rules for Service, Notification,
 * Dependency and ScheduledDowntime objects.
 *
 * Tip: Use `icinga2 object list --type Host` to
 * list all host objects after running
 * configuration validation (`icinga2 daemon -C`).
 */

/*
 * This is an example host based on your
 * local host's FQDN. Specify the NodeName
 * constant in `constants.conf` or use your
 * own description, e.g. "db-host-1".
 */

object Host NodeName {
  /* Import the default host template defined in `templates.conf`. */
  import "generic-host"

  /* Specify the address attributes for checks e.g. `ssh` or `http`. */
  address = "127.0.0.1"
  address6 = "::1"

  vars.groups = [ "vm", "monitoring" ]

  /* Define http vhost attributes for service apply rules in `services.conf`. */
  vars.http_vhosts["icingaweb2"] = {
    http_uri = "/authentication/login"
    http_vhost = "monitoring.fsfe.org"
    http_ssl = true
  }

  vars.disks_local["/"] = {
    disk_partition = "/"
  }
}

object Host "arsen.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "arsen.fsfeurope.org"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
  vars.disks["/mnt/pve/ceph-files"] = {
    disk_partition = "/mnt/pve/ceph-files"
  }
}

object Host "bayer.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "bayer.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["status"] = {
    http_vhost = "status.fsfe.org"
    http_ssl = true
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "berzelius.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "berzelius.fsfeurope.org"

  vars.groups = [ "vm", "ldap" ]

  vars.service["slapd"] = {
    service_to_check = "slapd"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "bunsen.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "bunsen.fsfeurope.org"
  address6 = "bunsen.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["apache2"] = {
    service_to_check = "apache2"
  }

  vars.http_vhosts["fsfe.org"] = {
    http_vhost = "fsfe.org"
    http_ssl = true
    http_address = "www1.fsfe.org"
  }
  vars.http_vhosts["www1.fsfe.org"] = {
    http_vhost = "www1.fsfe.org"
    http_ssl = true
  }
  vars.http_vhosts["test.fsfe.org"] = {
    http_vhost = "test.fsfe.org"
    http_ssl = true
    http_address = "www1.fsfe.org"
  }
  vars.http_vhosts["fsfeurope.org"] = {
    http_vhost = "fsfeurope.org"
    http_ssl = true
    http_address = "www1.fsfe.org"
  }
  vars.http_vhosts["ilovefs.org"] = {
    http_vhost = "ilovefs.org"
    http_ssl = true
  }
  vars.http_vhosts["freeyourandroid.org"] = {
    http_vhost = "freeyourandroid.org"
    http_ssl = true
  }
  vars.http_vhosts["pdfreaders.org"] = {
    http_vhost = "pdfreaders.org"
    http_ssl = true
  }
  vars.http_vhosts["drm.info"] = {
    http_vhost = "drm.info"
    http_ssl = true
  }

  vars.tor_vhosts["fsfe.org"] = {
    tor_vhost = "fsfeorg3hsfyuhmdylxrqdvgsmjeoxuuug5a4dv3c3grkxzsl33d3xyd.onion"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["log"] = {
    disk_partition = "/var/log"
  }
  vars.disks["srv"] = {
    disk_partition = "/srv"
  }
}

object Host "claus.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "claus.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["download"] = {
    http_vhost = "download.fsfe.org"
    http_ssl = true
  }

  vars.ssh_hosts["download.fsfe.org"] = {
    ssh_address = "download.fsfe.org"
    ssh_port = 10222
    ssh_ipv4 = true
    ssh_ipv6 = true
  }

  vars.tor_vhosts["download.fsfe.org"] = {
    tor_vhost = "download.fsfeorg3hsfyuhmdylxrqdvgsmjeoxuuug5a4dv3c3grkxzsl33d3xyd.onion"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["srv"] = {
    disk_partition = "/srv"
  }
}

object Host "conf.fsfe.org" {
  import "generic-host"
  import "ssh-agent"

  address = "conf.fsfe.org"
  address6 = "conf.fsfe.org"

  vars.groups = [ "vm" ]

  vars.service["bbb-web"] = {
    service_to_check = "bbb-web"
  }
  vars.service["etherpad"] = {
    service_to_check = "etherpad"
  }
  vars.service["redis-server"] = {
    service_to_check = "redis-server"
  }
  vars.service["freeswitch"] = {
    service_to_check = "freeswitch"
  }
  vars.service["nginx"] = {
    service_to_check = "nginx"
  }
  vars.service["docker"] = {
    service_to_check = "docker"
  }

  vars.http_vhosts["bbb"] = {
    http_vhost = "conf.fsfe.org"
    http_ssl = true
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/bigbluebutton"] = {
    disk_partition = "/var/bigbluebutton"
  }
}

object Host "cont1.noris.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "cont1.noris.fsfeurope.org"
  address6 = "cont1.noris.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["docker"] = {
    service_to_check = "docker"
    service_user = "dockeruser"
  }

  vars.service["caddy"] = {
    service_to_check = "caddy"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "cont2.noris.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "cont2.noris.fsfeurope.org"
  address6 = "cont2.noris.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["docker"] = {
    service_to_check = "docker"
    service_user = "dockeruser"
  }

  vars.service["caddy"] = {
    service_to_check = "caddy"
  }

  vars.dockerlogs["reuse-api"] = {
    container = "reuse-api"
    search_string = "WARNING"
    docker_user = "dockeruser"
    docker_binary = "/home/dockeruser/bin/docker"
    time = "39m"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "cont1.plutex.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "cont1.plutex.fsfeurope.org"
  address6 = "cont1.plutex.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["docker"] = {
    service_to_check = "docker"
    service_user = "dockeruser"
  }

  vars.service["caddy"] = {
    service_to_check = "caddy"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "curie.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "curie.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["mysqld"] = {
    service_to_check = "mysqld"
  }

  vars.http_vhosts["blogs"] = {
    http_uri = "/wp-login.php"
    http_vhost = "blogs.fsfe.org"
    http_ssl = true
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "davy.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "davy.fsfeurope.org"
  address6 = "davy.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["gitea"] = {
    service_to_check = "gitea"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var"] = {
    disk_partition = "/var"
  }
  vars.disks["/home"] = {
    disk_partition = "/home"
    disk_wfree = "10%"
    disk_cfree = "5%"
  }
  vars.disks["/tmp"] = {
    disk_partition = "/tmp"
  }

  vars.http_vhosts["gitea"] = {
    http_vhost = "git.fsfe.org"
    http_ssl = true
  }
}

object Host "debierne.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "debierne.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["nfs-server"] = {
    service_to_check = "nfs-server"
  }
  vars.service["wireguard vmbackup"] = {
    service_to_check = "wg-quick@vmbackup"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/srv/backup"] = {
    disk_partition = "/srv/backup"
  }
}

object Host "ekeberg.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "ekeberg.fsfeurope.org"
  address6 = "ekeberg.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/log"] = {
    disk_partition = "/var/log"
  }

}

object Host "htz4.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "htz4.fsfeurope.org"
  address6 = "htz4.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["matrix-synapse"] = {
    service_to_check = "matrix-synapse"
  }
  vars.service["postgresql"] = {
    service_to_check = "postgresql"
  }
  vars.service["nginx"] = {
    service_to_check = "nginx"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/mnt/HC_Volume_101172369"] = {
    disk_partition = "/mnt/HC_Volume_101172369"
  }

  vars.http_vhosts["matrix"] = {
    http_vhost = "matrix.fsfe.org"
    http_uri = "/health"
    http_string = "OK"
    http_ssl = true
  }
  vars.http_vhosts["chat"] = {
    http_vhost = "chat.fsfe.org"
    http_string = "<div id=\"matrixchat\""
    http_ssl = true
  }
}

object Host "fajans.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "fajans.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }

  vars.http_vhosts["community.fsfe.org"] = {
    http_vhost = "community.fsfe.org"
    http_ssl = true
  }

  vars.service["docker"] = {
    service_to_check = "docker"
  }
}

object Host "gahn.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "gahn.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["apache2"] = {
    service_to_check = "apache2"
  }

  vars.http_vhosts["fsfe.org"] = {
    http_vhost = "fsfe.org"
    http_ssl = true
    http_address = "www3.fsfe.org"
  }
  vars.http_vhosts["www3.fsfe.org"] = {
    http_vhost = "www3.fsfe.org"
    http_ssl = true
  }
  vars.http_vhosts["test.fsfe.org"] = {
    http_vhost = "test.fsfe.org"
    http_ssl = true
    http_address = "www2.fsfe.org"
  }
  vars.http_vhosts["fsfeurope.org"] = {
    http_vhost = "fsfeurope.org"
    http_ssl = true
    http_address = "www3.fsfe.org"
  }
	
  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "gallium.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "gallium.fsfeurope.org"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
  vars.disks["/mnt/pve/ceph-files"] = {
    disk_partition = "/mnt/pve/ceph-files"
  }
}

object Host "geoffroy.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "geoffroy.fsfeurope.org"
  address6 = "geoffroy.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["bind9"] = {
    service_to_check = "bind9"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }

  vars.dns["ip4"] = {
    domain = "fsfe.org"
    expected = "217.69.89.172"
    server = "188.172.205.115"
  }
  vars.dns["ip6"] = {
    domain = "fsfe.org"
    expected = "2001:aa8:ffed:f5f3::172"
    server = "2a00:11c0:d:1::115"
  }
}

object Host "germanium.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "germanium.fsfeurope.org"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
  vars.disks["/mnt/pve/ceph-files"] = {
    disk_partition = "/mnt/pve/ceph-files"
  }
}

object Host "ghiorso.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "ghiorso.fsfeurope.org"
  address6 = "ghiorso.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["prosody"] = {
    service_to_check = "prosody"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }

#  vars.disks["upload"] = {
#    disk_partition = "/var/lib/prosody/http_upload/"
#  }

  vars.ssl_cert["xmpp"] = {
    http_vhost = "jabber.fsfe.org"
    ssl_protocol = "xmpp"
    ssl_port = "5222"
  }
}

object Host "giesel.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "giesel.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["nextcloud"] = {
    http_vhost = "share.fsfe.org"
    http_ssl = true
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["log"] = {
    disk_partition = "/var/log"
  }
}

object Host "hope.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "hope.fsfeurope.org"
  address6 = "hope.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["collabora"] = {
    http_vhost = "collabora.fsfe.org"
    http_ssl = true
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }

  vars.service["nextcloud-spreed-signaling"] = {
    service_to_check = "nextcloud-spreed-signaling"
  }
}

object Host "htz2.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "htz2.fsfeurope.org"
  address6 = "htz2.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["meet"] = {
    http_vhost = "meet.fsfe.org"
    http_ssl = true
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }

  vars.service["jitsi-videobridge2"] = {
    service_to_check = "jitsi-videobridge2"
  }
}

object Host "iridium.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "iridium.fsfeurope.org"
  address6 = "iridium.fsfeurope.org"

  # DUS cluster has ICMP blocked
  vars.ping_address = "localhost"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
}

object Host "james.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "james.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["media.fsfe.org"] = {
    http_vhost = "media.fsfe.org"
    http_ssl = true
  }

  vars.service["peertube"] = {
    service_to_check = "peertube"
  }
  vars.service["nginx"] = {
    service_to_check = "nginx"
  }
  vars.service["postgresql"] = {
    service_to_check = "postgresql"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/log"] = {
    disk_partition = "/var/log"
  }
  vars.disks["/var/www/peertube"] = {
    disk_partition = "/var/www/peertube"
  }
}

object Host "kaim.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "kaim.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["innernet-server"] = {
    service_to_check = "innernet-server@fsfe"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "kalium.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "kalium.fsfeurope.org"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
  vars.disks["/mnt/pve/ceph-files"] = {
    disk_partition = "/mnt/pve/ceph-files"
  }
}

object Host "krypton.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "krypton.fsfeurope.org"
  address6 = "krypton.fsfeurope.org"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
}
object Host "list2.fsfe.org" {
  import "generic-host"
  import "ssh-agent"

  address = "list2.fsfe.org"
  address6 = "list2.fsfe.org"

  vars.groups = [ "vm", "mail" ]

  vars.service["postfix"] = {
    service_to_check = "postfix"
  }
  vars.service["nginx"] = {
    service_to_check = "nginx"
  }
  vars.service["mailman3"] = {
    service_to_check = "mailman3"
  }
  vars.service["mailman3-web"] = {
    service_to_check = "mailman3-web"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["spool"] = {
    disk_partition = "/var/spool/"
  }
  vars.disks["log"] = {
    disk_partition = "/var/log/"
  }
  vars.disks["archives"] = {
    disk_partition = "/var/lib/mailman/"
  }

  vars.http_vhosts["listinfo"] = {
    http_uri = "/postorius/lists/"
    http_vhost = "lists.fsfe.org"
    http_ssl = true
  }

  vars.http_vhosts["mailman.fsfeurope-listinfo"] = {
    http_uri = "/postorius/lists/"
    http_vhost = "mailman.fsfeurope.org"
    http_ssl = true
  }
}

object Host "vanadium.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "vanadium.fsfeurope.org"
  address6 = "vanadium.fsfeurope.org"

  vars.groups = [ "physical", "mail" ]

#  vars.service["postfix"] = {
#    service_to_check = "postfix"
#  }
#  vars.service["nginx"] = {
#    service_to_check = "nginx"
#  }
#  vars.service["mailman3"] = {
#    service_to_check = "mailman3"
#  }
#  vars.service["mailman3-web"] = {
#    service_to_check = "mailman3-web"
#  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
#  vars.disks["spool"] = {
#    disk_partition = "/var/spool/"
#  }
#  vars.disks["log"] = {
#    disk_partition = "/var/log/"
#  }
#  vars.disks["archives"] = {
#    disk_partition = "/var/lib/mailman/"
#  }
#
#  vars.http_vhosts["listinfo"] = {
#    http_uri = "/postorius/lists/"
#    http_vhost = "lists.fsfe.org"
#    http_ssl = true
#  }
#
#  vars.http_vhosts["mailman.fsfeurope-listinfo"] = {
#    http_uri = "/postorius/lists/"
#    http_vhost = "mailman.fsfeurope.org"
#    http_ssl = true
#  }
}

object Host "lithium.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "lithium.fsfeurope.org"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
  vars.disks["/mnt/pve/ceph-files"] = {
    disk_partition = "/mnt/pve/ceph-files"
  }
}

object Host "mail2.fsfe.org" {
  import "generic-host"
  import "ssh-agent"

  address = "mail2.fsfe.org"
  address6 = "mail2.fsfe.org"

  vars.groups = [ "vm", "mail" ]

  vars.service["postfix"] = {
    service_to_check = "postfix"
  }
  vars.service["rspamd"] = {
    service_to_check = "rspamd"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["spool"] = {
    disk_partition = "/var/spool/"
  }
  vars.disks["log"] = {
    disk_partition = "/var/log/"
  }

  vars.blacklist["ip4"] = {
    ip = "213.95.165.55"
  }
  vars.blacklist["ip6"] = {
    ip = "2001:780:215:1::55"
  }

  vars.email_delivery["forwarding"] = {
    email_mailto = "fred.bloggs@fsfe.org"
  }

  vars.ssl_cert["smtp-fsfe.org"] = {
    http_vhost = "mail.fsfe.org"
    ssl_protocol = "smtp"
    http_address = "mail2.fsfe.org"
  }
  vars.ssl_cert["smtp-mail2.fsfe.org"] = {
    http_vhost = "mail2.fsfe.org"
    ssl_protocol = "smtp"
  }
  vars.ssl_cert["smtp-fsfeurope.org"] = {
    http_vhost = "mail.fsfeurope.org"
    ssl_protocol = "smtp"
    http_address = "mail2.fsfe.org"
  }
}

object Host "mail3.fsfe.org" {
  import "generic-host"
  import "ssh-agent"

  address = "mail3.fsfe.org"
  address6 = "mail3.fsfe.org"

  vars.groups = [ "vm", "mail" ]

  vars.service["postfix"] = {
    service_to_check = "postfix"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["spool"] = {
    disk_partition = "/var/spool/"
  }
  vars.disks["log"] = {
    disk_partition = "/var/log/"
  }

  vars.blacklist["ip4"] = {
    ip = "31.24.147.90"
  }
  vars.blacklist["ip6"] = {
    ip = "2a02:16d0:1004:5a00:f5f3::90"
  }

  vars.email_delivery["forwarding"] = {
    email_mailto = "fred.bloggs@fsfe.org"
  }

  vars.ssl_cert["smtp-fsfe.org"] = {
    http_vhost = "mail.fsfe.org"
    ssl_protocol = "smtp"
    http_address = "mail3.fsfe.org"
  }
  vars.ssl_cert["smtp-mail3.fsfe.org"] = {
    http_vhost = "mail3.fsfe.org"
    ssl_protocol = "smtp"
  }
  vars.ssl_cert["smtp-fsfeurope.org"] = {
    http_vhost = "mail.fsfeurope.org"
    ssl_protocol = "smtp"
    http_address = "mail3.fsfe.org"
  }
}

object Host "redis1.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "redis1.fsfeurope.org"

  vars.groups = [ "vm", "mail" ]

  vars.service["redis_6379"] = {
    service_to_check = "redis_6379"
  }
}

object Host "redis2.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "redis1.fsfeurope.org"

  vars.groups = [ "vm", "mail" ]

  vars.service["redis_6379"] = {
    service_to_check = "redis_6379"
  }
}

object Host "welsbach.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "welsbach.fsfeurope.org"

  vars.groups = [ "vm", "mail" ]

  vars.service["dovecot"] = {
    service_to_check = "dovecot"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["mail"] = {
    disk_partition = "/var/mail/"
  }
  vars.disks["log"] = {
    disk_partition = "/var/log/"
  }

  vars.ssl_cert["imap-fsfe.org"] = {
    http_vhost = "imap.fsfe.org"
    ssl_protocol = "imaps"
    http_address = "imap.fsfe.org"
  }
}

object Host "richter.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "richter.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["rspamd"] = {
    service_to_check = "rspamd"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "richter2.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "richter2.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["rspamd"] = {
    service_to_check = "rspamd"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "urbain.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "urbain.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["nginx"] = {
    service_to_check = "nginx"
  }

  vars.http_vhosts["helpdesk.fsfe.org"] = {
    http_vhost = "helpdesk.fsfe.org"
    http_address = "helpdesk.fsfe.org"
    http_ssl = true
    http_uri = "/login"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "meitner.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "meitner.fsfeurope.org"
  address6 = "meitner.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "noddack.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "noddack.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["apache2"] = {
    service_to_check = "apache2"
  }

  vars.http_vhosts["fsfe.org"] = {
    http_vhost = "fsfe.org"
    http_ssl = true
    http_address = "www2.fsfe.org"
  }
  vars.http_vhosts["www2.fsfe.org"] = {
    http_vhost = "www2.fsfe.org"
    http_ssl = true
  }
  vars.http_vhosts["test.fsfe.org"] = {
    http_vhost = "test.fsfe.org"
    http_ssl = true
    http_address = "www2.fsfe.org"
  }
  vars.http_vhosts["fsfeurope.org"] = {
    http_vhost = "fsfeurope.org"
    http_ssl = true
    http_address = "www2.fsfe.org"
  }
  vars.http_vhosts["zangemann.com"] = {
    http_vhost = "zangemann.com"
    http_ssl = true
  }
  vars.http_vhosts["zangemann.de"] = {
    http_vhost = "zangemann.de"
    http_ssl = true
  }
  vars.http_vhosts["zangemann.org"] = {
    http_vhost = "zangemann.org"
    http_ssl = true
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["log"] = {
    disk_partition = "/var/log"
  }
  vars.disks["srv"] = {
    disk_partition = "/srv"
  }
}

object Host "ogawa.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "ogawa.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["apache2"] = {
    service_to_check = "apache2"
  }
  vars.service["tor"] = {
    service_to_check = "tor"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "osmium.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "osmium.fsfeurope.org"
  address6 = "osmium.fsfeurope.org"

  # DUS cluster has ICMP blocked
  vars.ping_address = "localhost"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
}

object Host "pauling.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "pauling.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["registration"] = {
    http_vhost = "registration.fsfe.org"
    http_ssl = true
    http_expect = "HTTP/1.1 404"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "platinum.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "platinum.fsfeurope.org"
  address6 = "platinum.fsfeurope.org"

  # DUS cluster has ICMP blocked
  vars.ping_address = "localhost"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
}

object Host "plumbum.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "plumbum.fsfeurope.org"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "proxy.noris.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "proxy.noris.fsfeurope.org"
  address6 = "proxy.noris.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["haproxy"] = {
    service_to_check = "haproxy"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "proxy.plutex.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "proxy.plutex.fsfeurope.org"
  address6 = "proxy.plutex.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["haproxy"] = {
    service_to_check = "haproxy"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "scheele.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "scheele.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["wiki"] = {
    http_vhost = "wiki.fsfe.org"
    http_ssl = true
  }
  vars.http_vhosts["wiki-test"] = {
    http_vhost = "wiki-test.fsfe.org"
    http_ssl = true
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "seaborg.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "seaborg.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["postgresql"] = {
    service_to_check = "postgresql"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["log"] = {
    disk_partition = "/var/log"
  }
  vars.disks["pg"] = {
    disk_partition = "/var/lib/postgresql"
  }
}

object Host "sodium.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "sodium.fsfeurope.org"

  vars.groups = [ "physical" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }
  vars.disks["/var/lib/vz"] = {
    disk_partition = "/var/lib/vz"
  }
  vars.disks["/mnt/pve/ceph-files"] = {
    disk_partition = "/mnt/pve/ceph-files"
  }
}

object Host "svedberg.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "svedberg.fsfeurope.org"
  address6 = "svedberg.fsfeurope.org"

  vars.groups = [ "container", "mail" ]

  vars.service["postgresql"] = {
    service_to_check = "postgresql"
  }
  vars.service["postfix"] = {
    service_to_check = "postfix"
  }
  vars.service["cron"] = {
    service_to_check = "cron"
  }

  vars.http_vhosts["tickets"] = {
    http_vhost = "tickets.fsfe.org"
    http_ssl = true
    http_uri = "/otrs/index.pl"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }

  /* Increase load limits */
  vars.warning = "1.50,1.40,1.30"
  vars.critical = "2.00,1.80,1.60"
}

object Host "tennant.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "tennant.fsfeurope.org"
  address6 = "tennant.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["bind9"] = {
    service_to_check = "bind9"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }

  vars.dns["ip4"] = {
    domain = "fsfe.org"
    expected = "217.69.89.172"
    server = "217.69.89.137"
  }
  vars.dns["ip6"] = {
    domain = "fsfe.org"
    expected = "2001:aa8:ffed:f5f3::172"
    server = "2001:aa8:ffed:f5f3::137"
  }
}

object Host "turn.fsfe.org" {
  import "generic-host"
  import "ssh-agent"

  address = "turn.fsfe.org"
  address6 = "turn.fsfe.org"

  vars.groups = [ "vm" ]

  vars.service["coturn"] = {
    service_to_check = "coturn"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "winkler.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address = "winkler.fsfeurope.org"
  address6 = "winkler.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.service["openvpn"] = {
    service_to_check = "openvpn"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "ramsay.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "ramsay.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.ssh_hosts["wrk1.api.reuse.software"] = {
    ssh_address = "wrk1.api.reuse.software"
    ssh_port = 11122
    ssh_ipv4 = true
    ssh_ipv6 = false
  }

  vars.service["docker"] = {
    service_to_check = "docker"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "vauquelin.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "vauquelin.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.http_vhosts["acme-dns.fsfe.org"] = {
    http_vhost = "acme-dns.fsfe.org"
    http_ssl = true
    http_uri = "/register"
    http_expect = "HTTP/1.1 405"
  }

  vars.service["acme-dns"] = {
    service_to_check = "acme-dns"
  }

  vars.disks["/"] = {
    disk_partition = "/"
  }
}

object Host "wood.fsfeurope.org" {
  import "generic-host"
  import "ssh-agent"

  address6 = "wood.fsfeurope.org"

  vars.groups = [ "vm" ]

  vars.disks["/"] = {
    disk_partition = "/"
  }

  vars.http_vhosts["drone.fsfe.org"] = {
    http_vhost = "drone.fsfe.org"
    http_ssl = true
  }

  vars.service["docker"] = {
    service_to_check = "docker"
  }
}


/* DOCKER WEBSITES */

object Host "Docker-Websites" {
  import "generic-host"

  address = "fsfe.org"

  vars.check_rdns = false

  vars.http_vhosts["api.reuse.software"] = {
    http_vhost = "api.reuse.software"
    http_ssl = true
  }

  vars.http_vhosts["discourse-news.fsfe.org"] = {
    http_vhost = "discourse-news.fsfe.org"
    http_ssl = true
    http_expect = "HTTP/1.1 401"
  }

  vars.http_vhosts["docs.fsfe.org"] = {
    http_vhost = "docs.fsfe.org"
    http_uri = "/sysadmin/backups"
    http_ssl = true
    http_expect = "HTTP/1.1 403"
  }

  vars.http_vhosts["drone.fsfe.org"] = {
    http_vhost = "drone.fsfe.org"
    http_ssl = true
  }

  vars.http_vhosts["forms.fsfe.org"] = {
    http_vhost = "forms.fsfe.org"
    http_ssl = true
    http_ipv4 = true
  }

  vars.http_vhosts["fossmarks.org"] = {
    http_vhost = "fossmarks.org"
    http_ssl = true
  }

  vars.http_vhosts["id.fsfe.org"] = {
    http_vhost = "id.fsfe.org"
    http_ssl = true
    http_uri = "/authorization"
    http_expect = "HTTP/1.1 400"
    http_ipv4 = true
  }

  vars.http_vhosts["keys.fsfe.org"] = {
    http_vhost = "keys.fsfe.org"
    http_ssl = true
    # ASCII key of openpgp-ca@fsfe.org (the CA)
    http_uri = "/openpgp-ca@fsfe.org.asc"
  }

  vars.http_vhosts["my.fsfe.org"] = {
    http_vhost = "my.fsfe.org"
    http_ssl = true
    http_ipv4 = true
  }

  vars.http_vhosts["nlformat.fsfe.org"] = {
    http_vhost = "nlformat.fsfe.org"
    http_ssl = true
    http_expect = "HTTP/1.1 401"
  }

  vars.http_vhosts["openpgpkey.fsfe.org"] = {
    http_vhost = "openpgpkey.fsfe.org"
    http_ssl = true
    # WKD key of openpgp-ca@fsfe.org (the CA)
    http_uri = "/.well-known/openpgpkey/fsfe.org/hu/ermf4k8pujzwtqqxmskb7355sebj5e4t"
  }

  vars.http_vhosts["pass.fsfe.org"] = {
    http_vhost = "pass.fsfe.org"
    http_ssl = true
  }

  vars.http_vhosts["pics.fsfe.org"] = {
    http_vhost = "pics.fsfe.org"
    http_ssl = true
  }
  vars.tor_vhosts["pics.fsfe.org"] = {
    tor_vhost = "pics.fsfeorg3hsfyuhmdylxrqdvgsmjeoxuuug5a4dv3c3grkxzsl33d3xyd.onion"
  }

  vars.http_vhosts["piwik.fsfe.org"] = {
    http_vhost = "piwik.fsfe.org"
    http_ssl = true
  }

  vars.http_vhosts["planet.fsfe.org"] = {
    http_vhost = "planet.fsfe.org"
    http_ssl = true
  }

  vars.http_vhosts["publiccode.eu"] = {
    http_vhost = "publiccode.eu"
    http_ssl = true
  }

  vars.http_vhosts["reuse.software"] = {
    http_vhost = "reuse.software"
    http_ssl = true
  }

  vars.http_vhosts["savecodeshare.eu"] = {
    http_vhost = "savecodeshare.eu"
    http_ssl = true
  }

  vars.http_vhosts["fedigov.eu"] = {
    http_vhost = "fedigov.eu"
    http_ssl = true
  }

  vars.http_vhosts["survey.fsfe.org"] = {
    http_vhost = "survey.fsfe.org"
    http_ssl = true
  }

  vars.http_vhosts["sharepic.fsfe.org"] = {
    http_vhost = "sharepic.fsfe.org"
    http_uri = "/health/backend"
    http_ssl = true
    http_sni = true
  }


  vars.http_vhosts["webpreview.fsfe.org"] = {
    http_vhost = "webpreview.fsfe.org"
    http_ssl = true
  }

  vars.http_vhosts["syshackerss.fsfe.org"] = {
    http_vhost = "syshackerss.fsfe.org"
    http_ssl = true
  }

  vars.http_vhosts["tedective.org"] = {
    http_vhost = "tedective.org"
    http_ssl = true
  }

}
