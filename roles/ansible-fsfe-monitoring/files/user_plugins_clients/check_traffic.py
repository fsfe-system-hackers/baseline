#!/usr/bin/env python3
# SPDX-FileCopyrightText: Free Software Foundation Europe e.V. <https://fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

"""Collect traffic data of the last 30 days and prepare them for Icinga input"""

import json
import subprocess
import sys


def get_default_iface():
    """Get default interface name (e.g. eth0)"""
    route = "/proc/net/route"
    interface = None
    with open(route, encoding="UTF-8") as stream:
        for line in stream.readlines():
            try:
                iface, dest, _, flags, _, _, _, _, _, _, _, =  line.strip().split()
                if dest != "00000000" or not int(flags, 16) & 2:
                    continue
                interface = iface
            except: # pylint: disable=bare-except
                interface = None

    # If the interface has not been found above, get the default route to an IP
    # and check which interface it takes
    if interface is None:
        iface = subprocess.run(
            # IP of proxy.noris.fsfeurope.org
            # pylint: disable=anomalous-backslash-in-string
            "ip route get 2001:780:215:1::54 | grep -Po '(?<=dev )(\\S+)'",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            check=True,
            shell=True,
        )
        return iface.stdout.strip()

    return interface


# Run vnstat with the detected interface, and output as json
vnstat_raw = subprocess.run(
    ["vnstat", "-i", get_default_iface(), "--json"],
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    universal_newlines=True,
    check=True,
)

data = json.loads(vnstat_raw.stdout)

# Get JSON version of vnstat output
jsonversion = data["jsonversion"]

rx, tx = [], []
try:
    # add traffic for each day to the list
    for daycount, day in enumerate(data["interfaces"][0]["traffic"]["days"]):
        rx.append(day["rx"])
        tx.append(day["tx"])
# "days" does not exist because only one day has been measured so far
# or it's a newer vnstat version where only "day" exists
except:  # pylint: disable=bare-except
    for daycount, day in enumerate(
        sorted(
            # Sort by id of day, and reverse it, to be able to get the last n days
            data["interfaces"][0]["traffic"]["day"], key=lambda d: d["id"], reverse=True
        )
    ):
        rx.append(day["rx"])
        tx.append(day["tx"])
        # Only count the last 30 days
        if daycount >= 29:
            break

# sum up the lists
rx = sum(rx)
tx = sum(tx)

# In jsonversion:2 the values are in bytes, while they are in KiB in 1
if jsonversion == "2":
    rx = round(rx / 1024)
    tx = round(tx / 1024)

# convert B to GB
rx_g = round(rx / 1024 ** 2, 2)
tx_g = round(tx / 1024 ** 2, 2)

print(
    f"RX: {rx_g}GiB, TX: {tx_g}GiB in the last {daycount + 1} days on {get_default_iface()} "
    f"| 'rx'={rx}KB 'tx'={tx}KB 'days'={daycount + 1}"
)

sys.exit(0)
