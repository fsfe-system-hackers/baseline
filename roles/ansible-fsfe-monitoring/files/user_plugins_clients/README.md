<!--
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2019 Free Software Foundation Europe
-->

This directory contains custom monitoring plugins meant to be used by ssh. They
must therefore be present on the monitored servers.
