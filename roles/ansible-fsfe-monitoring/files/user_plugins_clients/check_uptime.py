#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: Free Software Foundation Europe <https://fsfe.org>

"""
Show uptime of system and since when it's running.
Example: Uptime 2 days, 6:38:56 since 2021-03-30 09:34:24
"""

import datetime

def skip_ms(dt):
    """Remove the microseconds from a datetime, convert to string"""
    return str(dt).split(".")[0]

with open('/proc/uptime', 'r') as f:
    uptime_seconds = float(f.readline().split()[0])

uptime = skip_ms(datetime.timedelta(seconds=uptime_seconds))
start = skip_ms(datetime.datetime.now() - datetime.timedelta(seconds=uptime_seconds))

print("Uptime {uptime} since {start}".format(uptime=uptime, start=start))
