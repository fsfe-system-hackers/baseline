#!/usr/bin/env bash

# SPDX-FileCopyrightText: Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

#set -eo pipefail

#VARIABLES NAGIOS
OK=0
WARNING=1
CRITICAL=2
UNKNOWN=3

PROGNAME=$(basename "$0" .sh)

print_help() {
    echo "$PROGNAME is a Icinga plugin to check the logs of a Docker container for a specific string that indicates a bad state"
    echo ""
    echo "    -d <container_name>"
    echo "    -u <user> (User the Docker daemon runs as. Default: root"
    echo "    -s <search> (e.g. 'WARNING'. If found, Icinga will trigger a warning)"
    echo "    -b <docker_path> (path of docker command binary, if needed. Default: docker)"
    echo "    -t <time> (e.g. '10m' for last 10 minutes. Optional)"
    echo "    -w <warning number> (how many search hits trigger a warning status? Default: 1)"
    echo "    -c <critical number> (how many search hits trigger critical status? Default: 5)"
    echo "    -e <y or n> (Warn when logs are empty? Default: n)"
    exit $UNKNOWN
}

while getopts d:u:t:s:b:w:c:e:hv OPT; do
  case $OPT in
    d)  CONTAINER=$OPTARG;;
    u)  DOCKERUSER=$OPTARG;;
    t)  TIME=$OPTARG;;
    s)  SEARCH=$OPTARG;;
    b)  DOCKERBIN=$OPTARG;;
    w)  WARN_LIMIT=$OPTARG;;
    c)  CRIT_LIMIT=$OPTARG;;
    e)  EMPTY=$OPTARG;;
    h)  print_help;;
    *)  echo "Unknown option: -$OPTARG"; print_help;;
  esac
done

# Check for obligatory variables
if [[ -z ${CONTAINER} ]]; then
    echo "-c missing"
    print_help
fi
if [[ -z ${SEARCH} ]]; then
    echo "-s missing"
    print_help
fi

# Set defaults
if [[ -z ${TIME} ]]; then TIME="1970-01-01"; fi
if [[ -z $WARN_LIMIT ]]; then WARN_LIMIT=1; fi
if [[ -z $CRIT_LIMIT ]]; then CRIT_LIMIT=5; fi
if [[ -z $EMPTY ]]; then EMPTY=n; fi
if [[ -z $DOCKERBIN ]]; then DOCKERBIN=docker; fi

# Sanity: warn should not be higher than crit
if [[ $WARN_LIMIT -ge $CRIT_LIMIT ]]; then
    echo "Warning threshold must not be greater or equal than Critical threshold"
    print_help
fi

# Fetch logs of Docker container
if [[ ( -n ${DOCKERUSER}) && ( ${DOCKERUSER} != "root" ) ]]; then
    logs=$(env XDG_RUNTIME_DIR=/run/user/"$(id -u "${DOCKERUSER}")" DOCKER_HOST=unix:///run/user/"$(id -u "${DOCKERUSER}")"/docker.sock sudo --preserve-env=XDG_RUNTIME_DIR,DOCKER_HOST -u "${DOCKERUSER}" ${DOCKERBIN} logs --since "${TIME}" "${CONTAINER}" 2>&1)
else
    logs=$(${DOCKERBIN} logs --since "${TIME}" "${CONTAINER}" 2>&1)
fi

if [[ $? != 0 ]]; then
    echo "UNKNOWN: Fetching of logs of Docker container ${CONTAINER} was not successful. Does it exist?"
    exit $UNKNOWN
fi

if [[ -n ${logs} ]]; then
    # Grep logs for search word
    hits=$(grep "$SEARCH" <<< "$logs")
    # only count hit lines if there was actually a hit. Avoids counting empty lines
    if [[ -n $hits ]]; then
        hitlines=$(wc -l <<< "$hits")
    else
        hitlines=0
    fi

    # Exit based on thresholds
    if [[ $hitlines -ge $CRIT_LIMIT ]]; then
        echo "CRITICAL: Logs of Docker container ${CONTAINER} contain ${hitlines} worrisome entries"
        echo
        echo "$hits"
        exit $CRITICAL
    elif [[ $hitlines -ge $WARN_LIMIT ]]; then
        echo "WARNING: Logs of Docker container ${CONTAINER} contain ${hitlines} worrisome entries"
        echo
        echo "$hits"
        exit $WARNING
    elif [[ ($hitlines -lt $WARN_LIMIT) && ($hitlines != 0) ]]; then
        echo "OK: Logs of Docker container ${CONTAINER} contain ${hitlines} worrisome entries"
        exit $OK
    else
        echo "OK: No worrisome log entries in Docker container ${CONTAINER}"
        exit $OK
    fi
else
    if [[ $EMPTY == "y" ]]; then
        echo "UNKNOWN: Logs of Docker container ${CONTAINER} are empty"
        exit $UNKNOWN
    else
        echo "OK: Logs of Docker container ${CONTAINER} are empty"
        exit $OK
    fi
fi
