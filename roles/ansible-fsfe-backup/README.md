<!--
SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: MIT
-->

# FSFE Borg backup role

This playbook and role configures backups on the FSFE's servers. It is based on
[FiaasCo's borgbackup](https://github.com/FiaasCo/borgbackup/) which also cares
about configuring the borg *server* part, but this repo is only tested and used
to configure the *clients*. It does the following:

* install borg and a SMTP server
* generate SSH key and store it on the remote storage
* create a borg backup repository on the remote storage
* setup wrapper script and cronjob on server

Since July 2019, we use borg to backup most of our servers. Before that, we used
custom scripts running on the server "cleve". On this page, the setup is
described, and you will find information on how to add a new server to the
backup process, and restore data if necessary.

The principle is simple: Each servers cares for its own backups, and can only
access its own backups. Every night, a cron job triggers a wrapper script
(`borg-wrapper`) which runs borg to make a backup to a remote storage. The
server identifies on the remote storage with an SSH key. With this key, the
server can only modify its own backups.

A log file for each backup cron run is sent to the FSFE `admin+backup@` address.
It contains the `--stats --verbose` output of the borg create and prune
commands, as well as the output of any pre- or post-backup commands, by default
a list of all current borg archives.

We automatically run regular snapshots of the complete remote storage which
cannot be modified by the individual servers. We also use a sub-account to
restrict access to other directories on the remote storage. This way, we have
some safety if the server is compromised.

## Important directories and files

* In `/host_vars/`, you will find the variables for each client. It mostly
  contains the borg passphrase, but can also overwrite default values
* In `/group_vars/`, some FSFE-specific default values for all clients are
  provided, e.g. the backup location.
* In `roles/borgbackup/defaults/main.yml`, all other default values are given.

## Setup locally

Ansible 2.4 or higher is required to run this role.

Update the [inventory
submodule](https://git.fsfe.org/fsfe-system-hackers/inventory) to reflect the
newest changes to the list of our hosts and the groups that they are in:
```sh
git submodule update --remote inventory
```

## Add a new server

If you created a new server, please configure its backup immediately with this
repo!

To add the new server, add the server's FQDN (e.g. `server.fsfe.org`) to the
file `inventory` in the client section.

Afterwards, run `ansible-vault create host_vars/server.fsfe.org` to create the
**configuration for the new server**. In the new file, insert the new password
used to encrypt backups:

```yaml
---
borgbackup_passphrase: aLongAndSecurePassword
```

Note that the password for these encrypted files is stored in `vaultpw.pgp`, a
GPG encrypted file which only a few people have access to. Please only add new
passwords (and servers) if you can open this file. In the new file, you can also
overwrite default variables.

Now you can **run the playbook** as described in the README.md of the actual
playbook (this has been turned into a role). Please note that this role runs
host-by-host and not task-by-task to avoid a problem with the local modification
of the `authorized_keys` file.

If everything went fine, please **commit and push** your changes to Git!

### Run scripts before backup

It may be useful to run commands right before the backup, e.g. to dump databases.

By default, the wrapper script runs `/root/bin/backup.sh` before the backup, and
`/root/bin/post-backup.sh` after the backup, given that they exist. Add your
commands in these script files and make them executable. For example, it could
look like this:

```sh
#!/bin/sh
echo "Dump Postgres databases"
sudo -u fsfe -- sh -c "cd / ; pg_dump > /home/fsfe/postgres_dump.sql"
```

The output will be mailed with the borg logs to the specified email address if
there was at least one error in the backup process.

### Tricks

* To avoid that you have to type in the remote storage's password for some
  operations, add your SSH key to the `authorized_keys` file.
* The playbook configures the cron job to send an email. In order to test
  whether your server actually sends emails correctly, run `ls | sendmail
  youremail@example.com`. If nothing arrives in your mail account (also check
  spam!), please debug.


## Restore from backup

In order to restore content of a specific borg archive, you can mount it using
the wrapper script:

1. Create a mountpoint: `mkdir /mnt/borg`
2. Find the archive you would like to mount: `borg-wrapper list`
3. Mount the archive to the local mountpoint, e.g.: `borg-wrapper mount
   20190716-1255 /mnt/borg`

Now, you can search and copy the desired data. Afterwards, please unmount the
mointpoint with `umount /mnt/borg`.

Extraction of a complete archive is not supported by the wrapper script as of
today. But by inspecting its content in `/usr/local/bin/borg-wrapper`, it should
be fairly easy to build the command for this.


## Uninstall backup

To remove all files generated by the Ansible playbook, e.g. in order to disable
backups of a client, follow these steps:

* On the client, remove the files `/etc/cron.d/borg-backup`,
  `/root/.borg.passphrase`, `/root/.ssh/id_borg_rsa*`, and
  `/usr/local/bin/borg-wrapper`
* On the client, clear the modified file `/root/.ssh/config` from the part added
  by the playbook. If that's the only content of the file, you can remove it
  entirely.
* On the remote storage, remove the client's SSH key from the
  `.ssh/authorized_keys` file. Please note to take the correct file, from the
  main account's root directory it is `/BorgBackup/.ssh/authorized_keys`.
* On the remote storage, delete the client's directory if you want to remove all
  backups as well. From the main account's root directory it is
  `/BorgBackup/servers/server.fsfe.org`. However, it is recommended to not
  delete the old backups of the server was used in production in case someone
  needs some information after the deletion.


## Troubleshooting

### Mail from cron job not sent

This can happen both with exim4 and postfix:

* **exim4**: In the log files you could see `R=nonlocal: Mailing to remote
  domains not supported`. To solve it, either configure exim to support remote
  domains (`dc_eximconfig_configtype='internet'`) or use a relay server for
  those, or install `nullmailer` instead if exim4 is not used for anything else.
  This program is much lighter and works by default.
* **postfix**: The log files show `<root@server.localdomain>: Sender address
  rejected: Domain not found` which is a sign that the receiving (or relaying)
  mail server blocked the email because the host's domain is invalid. You can
  solve this by manually setting the domain postfix shall use: `mydomain =
  fsfeurope.org` (or fsfe.org, depending on the FQDN of the server).

## License and Copyright

Licensed under MIT. This repo is REUSE compliant.

## Further reading

* [Borg documentation](https://borgbackup.readthedocs.io/en/stable/)
